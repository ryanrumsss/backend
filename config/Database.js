import mysql from "mysql2";

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1q2w3e4r5t',
    database: 'vascomm_db'
});

export default db;
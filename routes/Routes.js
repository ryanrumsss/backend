import express from "express";
import {showContents, showContentBySlug, createContent, updateContent, deleteContent} from "../controllers/Content.js";

const router = express.Router();

router.get('/contents', showContents);
router.get('/contents/:slug', showContentBySlug);
router.post('/contents', createContent);
router.put('/contents/:slug', updateContent);
router.delete('/contents/:slug', deleteContent);

export default router;
import {
    addVisitCount,
    getContentBySlug,
    getContents,
    insertContent,
    updateContentBySlug,
    deleteContentBySlug
} from "../models/contentModel.js";
import moment from "moment";

export const showContents = (req, res) => {
    getContents((err, results) => {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json(results);
        }
    });
}

export const showContentBySlug = (req, res) => {
    const slug = req.params.slug;
    addVisitCount(slug);
    getContentBySlug(slug, (err, results) => {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json(results);
        }
    });
}

export const createContent = (req, res) => {
    const data = req.body;
    const created_at = moment().format('YYYY-MM-DD hh:mm:ss');
    data.slug = titleToSlug(data.title);
    data.created_at = created_at;
    data.updated_at = created_at;
    data.visit_count = 0;
    insertContent(data, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.status(200).json(results);
        }
    });
}

export const updateContent = (req, res) => {
    const data = req.body;
    console.log(data.title)
    data.slug = titleToSlug(data.title);

    const slug = req.params.slug;
    updateContentBySlug(data, slug, (err, results) => {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json(results);
        }
    });
}

export const deleteContent = (req, res) => {
    const slug = req.params.slug;
    deleteContentBySlug(slug, (err, results) => {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json(results);
        }
    });
}

const titleToSlug = title => {
    let slug;

    slug = title.toLowerCase();
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    slug = slug.replace(/ /gi, "-");
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    return slug;
};
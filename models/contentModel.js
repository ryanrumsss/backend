import Database from "../config/Database.js";
import moment from "moment";

export const getContents = result => {
    Database.query("SELECT * FROM contents", (error, results) => {
        if(error) {
            console.log(error);
            result(error, null);
        } else {
            result(null, results);
        }
    })
}

export const getContentBySlug = (slug, result) => {
    Database.query("SELECT * FROM contents WHERE slug = ?", [slug], (error, results) => {
        if(error) {
            console.log(error);
            result(error, null);
        } else {
            result(null, results[0]);
        }
    })
}

export const insertContent = (data, result) => {
    Database.query("INSERT INTO contents SET ?", [data], (error, results) => {
        if(error) {
            console.log(error);
            result(error, null);
        } else {
            result(null, results);
        }
    })
}

export const updateContentBySlug = (data, slug, result) => {
    const updated_at = moment().format('YYYY-MM-DD hh:mm:ss');
    Database.query("UPDATE contents SET title = ?, body = ?, slug = ?, updated_at = ? WHERE slug = ?", [data.title, data.body, data.slug, updated_at, slug], (error, results) => {
        if(error) {
            console.log(error);
            result(error, null);
        } else {
            result(null, results);
        }
    })
}

export const deleteContentBySlug = (slug, result) => {
    Database.query("DELETE FROM contents WHERE slug = ?", [slug], (error, results) => {
        if(error) {
            console.log(error);
            result(error, null);
        } else {
            result(null, results);
        }
    })
}

export const addVisitCount = (slug) => {
    const updated_at = moment().format('YYYY-MM-DD hh:mm:ss');
    Database.query("UPDATE contents SET visit_count = visit_count + 1, updated_at = ? WHERE slug = ?", [updated_at, slug], (error, results) => {
        if(error) {
            console.log(error);
        } else {
            console.log(results);
        }
    })
}